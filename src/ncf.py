import tensorflow as tf
import argparse
import os
import numpy as np
import json
from tensorflow.keras.utils import to_categorical

# for data processing
def _load_training_data(base_dir):
    """ load training data """
    df_train = np.load(os.path.join(base_dir, 'train.npy'))
    user_train, movie_train, y_train = np.split(np.transpose(df_train).flatten(), 3)
    return user_train, movie_train, y_train


def _get_user_embedding_layers(inputs, emb_dim):
    """ create user embeddings """
    user_gmf_emb = tf.keras.layers.Dense(emb_dim, activation='relu')(inputs)

    user_mlp_emb = tf.keras.layers.Dense(emb_dim, activation='relu')(inputs)

    return user_gmf_emb, user_mlp_emb


def _get_movie_embedding_layers(inputs, emb_dim):
    """ create movie embeddings """
    movie_gmf_emb = tf.keras.layers.Dense(emb_dim, activation='relu')(inputs)

    movie_mlp_emb = tf.keras.layers.Dense(emb_dim, activation='relu')(inputs)

    return movie_gmf_emb, movie_mlp_emb


def _gmf(user_emb, movie_emb):
    """ general matrix factorization branch """
    gmf_mat = tf.keras.layers.Multiply()([user_emb, movie_emb])

    return gmf_mat


def _mlp(user_emb, movie_emb, dropout_rate):
    """ multi-layer perceptron branch """
    def add_layer(dim, input_layer, dropout_rate):
        hidden_layer = tf.keras.layers.Dense(dim, activation='relu')(input_layer)

        if dropout_rate:
            dropout_layer = tf.keras.layers.Dropout(dropout_rate)(hidden_layer)
            return dropout_layer

        return hidden_layer

    concat_layer = tf.keras.layers.Concatenate()([user_emb, movie_emb])

    dropout_l1 = tf.keras.layers.Dropout(dropout_rate)(concat_layer)

    dense_layer_1 = add_layer(64, dropout_l1, dropout_rate)

    dense_layer_2 = add_layer(32, dense_layer_1, dropout_rate)

    dense_layer_3 = add_layer(16, dense_layer_2, None)

    dense_layer_4 = add_layer(8, dense_layer_3, None)

    return dense_layer_4


def _neuCF(gmf, mlp, dropout_rate):
    concat_layer = tf.keras.layers.Concatenate()([gmf, mlp])

    output_layer = tf.keras.layers.Dense(6, activation='softmax')(concat_layer)

    return output_layer


def build_graph(user_dim, movie_dim, dropout_rate=0.25):
    """ neural collaborative filtering model """

    user_input = tf.keras.Input(shape=(user_dim))
    movie_input = tf.keras.Input(shape=(movie_dim))

    # create embedding layers
    user_gmf_emb, user_mlp_emb = _get_user_embedding_layers(user_input, 32)
    movie_gmf_emb, movie_mlp_emb = _get_movie_embedding_layers(movie_input, 32)

    # general matrix factorization
    gmf = _gmf(user_gmf_emb, movie_gmf_emb)

    # multi layer perceptron
    mlp = _mlp(user_mlp_emb, movie_mlp_emb, dropout_rate)

    # output
    output = _neuCF(gmf, mlp, dropout_rate)

    # create the model
    model = tf.keras.Model(inputs=[user_input, movie_input], outputs=output)

    return model


def model(x_train, y_train, n_user, n_movie, num_epoch, batch_size):

    #num_batch = np.ceil(x_train[0].shape[0]/batch_size)

    # build graph
    model = build_graph(n_user, n_movie)

    # compile and train
    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-3)

    model.compile(optimizer=optimizer,
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False),
                  metrics=['accuracy'])

    model.fit(
        x=x_train,
        y=y_train,
        batch_size=batch_size,
        verbose=2,
        epochs=num_epoch
    )

    return model


def _parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--model_dir', type=str)
    parser.add_argument('--sm-model-dir', type=str, default=os.environ.get('SM_MODEL_DIR'))
    parser.add_argument('--train', type=str, default=os.environ.get('SM_CHANNEL_TRAINING'))
    parser.add_argument('--hosts', type=list, default=json.loads(os.environ.get('SM_HOSTS')))
    parser.add_argument('--current-host', type=str, default=os.environ.get('SM_CURRENT_HOST'))
    parser.add_argument('--epochs', type=int, default=3)
    parser.add_argument('--batch_size', type=int, default=256)
    parser.add_argument('--n_user', type=int)
    parser.add_argument('--n_movie', type=int)

    return parser.parse_known_args()


if __name__ == "__main__":
    args, unknown = _parse_args()

    # load data
    user_train, movie_train, train_labels = _load_training_data(args.train)
    user_train = to_categorical(user_train, dtype=tf.int8)
    movie_train = to_categorical(movie_train, dtype=tf.int8)
    # build model
    ncf_model = model(
        x_train=[user_train, movie_train],
        y_train=train_labels,
        n_user=args.n_user,
        n_movie=args.n_movie,
        num_epoch=args.epochs,
        batch_size=args.batch_size
    )

    if args.current_host == args.hosts[0]:
        # save model to an S3 directory with version number '00000001'
        ncf_model.save(os.path.join(args.sm_model_dir, '000000001'), 'neural_collaborative_filtering.h5')